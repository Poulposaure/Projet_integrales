#!/usr/bin/ruby

# Projet S3

################################################################################

def rectangles_gauches(a, b, n)
  """calcule une integrale selon la methode des rectangles gauches"""
  somme = 0
  h = (b - a) / n
  p = a
  for k in 0..(n - 1)
    somme += fonction(p)
    p += h
  end# while
  return somme * h
end# rectangles_gauches

def rectangles_droits(a, b, n)
  """calcule une integrale selon la methode des rectangles droits"""
  somme = 0
  h = (b - a) / n
  p = a + h
  for k in 1..n
    somme += fonction(p)
    p += h
  end# while
  return somme * h
end# rectangles_gauches


def rectangles_medians(a, b, n)
  """calcule une integrale selon la methode des rectangles medians"""
  somme = 0
  h = (b - a) / n
  p = a
  for k in 0..(n - 1)
    x = (2.0 * p + h) / 2.0
    somme += fonction(x)
    p += h
  end
  somme *= h
  return somme
end

def methode_trapezes(a, b, n)
  """calcule une integrale selon la methode des trapezes"""
  somme = 0
  h = (b - a) / n
  p = a + h
  for i in 1..(n - 1)
     somme += fonction(p)
     p += h
  end
  somme *= 2
  somme += fonction(a) + fonction(b)#
  return somme * h * 0.5
end

def methode_simpson(a, b, n)
  somme = 0
  somme2 = 0
  h = (b - a) / n
  for k in 1..(n - 1)
    x = k * h + a
    somme += fonction(x)
  end
  for k in 0..(n - 1)
    x = 0.5 * h * (2 * k + 1) + a
    somme2 += fonction(x)
  end
  somme = 2 * somme + 4 * somme2
  somme += fonction(a) + fonction(b)
  somme *= (b - a) / (6.0 * n)
  return somme
end

def valeur_probabilite(m, s, d, t, n)
  """renvoie la valeur de la probabilite P(X < t) pour t >= 0 lorsque X suit
  une loi normale de parametres m et s > 0 fourni par l'utilisateur"""
  somme = somme2 = 0
  if d <= -9
    a = 0
  else
    a = d
  end
  b = t
  h = (b - a) / n
  f = 1.0 / (s * Math.sqrt(2 * Math::PI))
  for k in 1..(n - 1)
    x = h * k + a
    fx = -0.5 * ((x - m) / s) ** 2
    somme += f * fonction(fx)
  end
  for k in 0..(n - 1)
    x = 0.5 * h * (2 * k + 1) + a
    fx = -0.5 * ((x - m) / s) ** 2
    somme2 += f * fonction(fx)
  end
  somme = 2 * somme + 4 * somme2
  fa = -0.5 * ((a - m) / s) ** 2
  fb = -0.5 * ((b - m) / s) ** 2
  somme += f * (fonction(fa) + fonction(fb))
  somme *= h / 6.0
  if d <= -9
    return somme + 0.5
  end
  return somme
end

def fonction(var)
    return 1.0 / (1.0 + var**2)
end

################################################################################

def main
  s11 = rectangles_gauches(0.0, 1.0, 10.0)# marche, 0,285
  print "Exercice 1.1 : S = ", s11, "\n"
  # Exercice 1.2
  # valeur reelle = 1 / 3.0
  
  s2 = rectangles_droits(0.0, 1.0, 10.0)#marche, 0.385
  print "Exercice 2 : S = ", s2, "\n"

  #s3 = rectangles_medians(0.0, 1.0, 10.0)#marche, 0.3325
  #print "Exercice 3 : S = ", s3, "\n\n"

  #print "Exercice 4.1 : S = ", s3, "\n"#ok, 0,24875
  #s = rectangles_medians(-(Math::PI / 2.0), (Math::PI / 2.0), 10.0)
  #print "Exercice 4.2 : S = ", s, "\n"#ok, O
  #s43 = rectangles_medians(1.0, (Math::E), 10.0)
  #print "Exercice 4.3 : S = ", s43, "\n"#ok, 1.0007756439331978

  #s = methode_trapezes(0.0, 1.0, 10.0)
  #print "Exercice 5.a : S = ", s, "\n"#ok, 0.25249999999999995
  #s = methode_trapezes(0.0, (Math::PI / 2.0), 10.0)
  #print "Exercice 5.b : S = ", s, "\n"# ok, 0.9979429863543573
  s = methode_trapezes(0.0, 50.0, 10.0)#ok, 2.5339182745315214
  print "Exercice 5.c : S = ", s, "\n"#1000, 1.0002083246532936

  #s6 = methode_simpson(0.0, 1.0, 10.0)
  #print "Exercice 6.1 : S = ", s6, "\n"#ok, 0,3333333333
  s6 = methode_simpson(-3.0, 3.0, 10.0)
  print "Exercice 6.2.a : S = ", s6, "\n"#1.772413104492015
  #print "Exercice 6.2.b : S = ", s6, "\n"#2.4980271603696202

  s7 = valeur_probabilite(0.0, 1.0, -9.0, 1.23, 10.0)
  print "Exercice 7.1 : S = ", s7, "\n"#0.890651474810393
  s72 = valeur_probabilite(0.0, 1.0, -1.0, 1.0, 10.0)
  print "Exercice 7.12 : S = ", s72, "\n"#0.68269003177694
end
    
def main2
  # BORNES INFERIEURE, SUPERIEURE, SUBDIVISION
  rg = rectangles_gauches(0.0, 1.0, 10.0)
  print "RECTANGLE GAUCHE : S = ", rg, "\n"
  
  rd = rectangles_droits(0.0, 1.0, 10.0)
  print "RECTANGLE DROIT : S = ", rd, "\n"

  rm = rectangles_medians(1.0, Math::E, 10.0)
  print "RECTANGLE MEDIAN : S = ", rm, "\n\n"

  mt = methode_trapezes(0.0, 50, 10.0)
  print "METHODE TRAPEZES : S = ", mt, "\n"

  ms = methode_simpson(-3.0, 3.0, 10.0)
  print "METHODE SIMPSON : S = ", ms, "\n"

  # M, SIGMA, BORNES INFERIEURE, SUPERIEURE, SUBDIVISION
  vp = valeur_probabilite(0.0, 1.0, -9.0, 1.23, 10.0)
  print "PROBABILITE : S = ", vp, "\n"
end

################################################################################

#main
main2